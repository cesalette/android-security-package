package com.example.testsecuritypackage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class javaMac extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_mac);
        try {
            javaMacTest();
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public void javaMacTest() throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException {
        /*int a;
        Mac mac = Mac.getInstance("HmacSHA256");
        byte[] keyBite = new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        String algorithm = "RawBytes";
        SecretKeySpec keySpec = new SecretKeySpec(keyBite, algorithm);
        mac.init(keySpec);

        byte[] data = "CedricCrypto".getBytes("UTF-8");
        byte[] data1 = "Cedric".getBytes("UTF-8");
        byte[] data2 = "Crypto".getBytes("UTF-8");
        byte[] macBytes = mac.doFinal(data);
        mac.update(data1);
        byte[] macBytes2 = mac.doFinal(data2);
        // IS MAC = Hash + Encrypt
        byte[] dataDigest = "abcdef".getBytes("UTF-8");
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        byte[] encrypt = cipher.doFinal(data);
        byte[] digest = messageDigest.digest(encrypt);
        a = 35;*/


        Mac mac = Mac.getInstance("HmacSHA256");
        byte[] keyBite = new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        String algorithm = "RawBytes";
        SecretKeySpec keySpec = new SecretKeySpec(keyBite, algorithm);
        mac.init(keySpec);

        byte[] data = "CedricCrypto".getBytes("UTF-8");
        byte[] macBytes = mac.doFinal(data);

        // IS MAC = Hash + Encrypt
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        byte[] digest = messageDigest.digest(data);
        byte[] encrypt = cipher.doFinal(digest);
        int a = 56;

    }
}