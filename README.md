To test the different activity you need to set them as the launcher activity.

For this go to app/manifest/AndroidManifest.xml

The activity containing the followings tags is the launcher activity: 

	<intent-filter>
		<action android:name="android.intent.action.MAIN" />

		<category android:name="android.intent.category.LAUNCHER" />
	</intent-filter>
			
Indeed, to change the launcher activity, copy paste it to the activity you want to try, and delete it from the original activity.


Note : If 2 activity have this tags, this is the activityt the higher in the file that is gonna launch.
